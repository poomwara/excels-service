# Welcome to Export excels service

This API create by flask using python programming

# Installation

- `git clone https://warawatlailerd@bitbucket.org/poomwara/excels-service.git `
- `docker build -t python-export-excels .`
- `docker compose up -d`

# URL to call

- `[POST] : http://localhost:5000/excels/export`

# Data to upload

- `const data = { data : [] }`

# How to call API

```
const config = { "responseType" : "blob" };
const res = await axios.post(endpoint, data, config)
saveAs(res.data, filename)
```

> **Note:** The Export excels service is beta version.
