from flask import Flask, send_file, request, after_this_request
from flask_cors import CORS
import pandas as pd
import uuid
import os


app = Flask(__name__)
CORS(app)

def to_excels(id, data):    
    df = pd.DataFrame(data)
    df.to_excel(f"./data/{id}.xlsx", index=False)
    

@app.route('/excels/export', methods=['POST'])
def export():
    body = request.json         
    data_to_export = body['data']    
    id = uuid.uuid4()    
    to_excels(id, data_to_export)    
    
    @after_this_request
    def remove_file(response):
        try:
            os.remove(f"./data/{id}.xlsx")
        except Exception as error:
            app.logger.error("Error removing or closing downloaded file handle", error)
            
        return response
    
    return send_file(f"./data/{id}.xlsx", mimetype="application/application/xlsx")


if __name__ == '__main__':
    app.run()